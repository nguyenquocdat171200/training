<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

//decryption
if (!function_exists('myDecrypt')) {
    function myDecrypt($string, $key)
    {
        $result = '';
        $string = base64_decode($string);
        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keyChar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) - ord($keyChar));
            $result .= $char;
        }
        return $result;
    }
}

//encode
if (!function_exists('myEncrypt')) {
    function myEncrypt($string, $key)
    {
        $result = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keyChar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) + ord($keyChar));
            $result .= $char;
        }
        return base64_encode($result);
    }
}

if (!function_exists('array_get')) {
    function array_get($array, $key, $default = null)
    {
        return Arr::get($array, $key, $default);
    }
}

if (!function_exists('frontendGuard')) {
    function frontendGuard()
    {
        return Auth::guard('frontend');
    }
}

if (!function_exists('backendGuard')) {
    function backendGuard()
    {
        return Auth::guard('admin');
    }
}

if (!function_exists('backendGetCurrentUser')) {
    function backendGetCurrentUser()
    {
        return backendGuard()->user();
    }
}

if (!function_exists('backendGetCurrentUserId')) {
    function backendGetCurrentUserId()
    {
        return Auth::guard('admin')->user()->id;
    }
}

if (!function_exists('getConstant')) {
    function getConstant($key,$default = null)
    {
        return Config::get('constant.' . $key,$default);
    }
}