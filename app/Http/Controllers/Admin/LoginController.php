<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //form login
    public function formLoginUser()
    {
        return view('admin.login');
    }

    //login admin
    public function loginUser(Request $request)
    {
        $data = $request->only('email', 'password');
        if (Auth::attempt($data)) {
            $user = backendGetCurrentUser();
            switch ($user['role_type']) {
                case 0 :
                    return redirect()->route('indexBackend');
                    break;
                case 1 :
                    return redirect()->route('index');
                    break;
                case 2 :
                    return redirect()->route('index');
                    break;
            }
        } else {
            return back()->with('error', trans('message.checkLogin'));
        }
    }

    //logout
    public function logOut(Request $request)
    {
        backendGuard()->logout();
        return view('admin.login');
    }
}
