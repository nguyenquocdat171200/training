<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoneAdminRequest;
use App\Model\Entities\Admin;
use App\Repositories\Admin\AdminRepository;
use App\Validate\Admin\UserValidator;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    protected $adminRepository;
    protected $adminUpload;
    protected $validate;

    const SESSION_STORE = 'SESSION_STORE';

    public function __construct(AdminRepository $adminRepository, \App\Services\AdminUpload $adminUpload,UserValidator $validate)
    {
        phpinfo();
        $this->adminRepository = $adminRepository;
        $this->adminUpload = $adminUpload;
        $this->validate = $validate;
    }

    public function search(Request $request)
    {
        $admin = backendGetCurrentUser();
        if ($admin->can('search', Admin::class)) {
            $keyword = $request->keyword;
            $condition = [
                ['name', 'like', '%' . $keyword . '%'],
                ['del_flag', getConstant('DEL_FLAG_ON')]
            ];
            $data = $this->adminRepository->paginate($condition, getConstant('PAGINATE'));
            return view('admin.pages.admin.all', compact('data'));
        }
    }


    public function index(Request $request)
    {
        $admin = backendGetCurrentUser();
        if ($admin->can('index', Admin::class)) {
            if ($request->session()->has('SESSION_STORE')) {
                session()->forget(self::SESSION_STORE);
            }
            $condition = [
                ['del_flag', getConstant('DEL_FLAG_ON')]
            ];
            return view('admin.pages.admin.all', (['data' => $this->adminRepository->paginate($condition, getConstant('PAGINATE'))]));
        }
    }

    public function create(Request $request)
    {
        $admin = backendGetCurrentUser();
        if ($admin->can('create', Admin::class)) {
            $action = url('/backend/users/create');
            return view('admin.pages.admin.add_edit', compact('action'));
        }
    }

    public function store(Request $request)
    {
        $dataRequest = $request->all();
        $rules = $this->validate->rules;
        if (!array_key_exists('avatar', $dataRequest)) {
            unset($rules['avatar']);
        }
        $validator = Validator::make($dataRequest,$rules);
        if ($validator->fails()) {
            session()->put(self::SESSION_STORE, $dataRequest);
            $img = $this->adminUpload->uploadImg();
            if (!empty($img)) { //ảnh tồn tại
                $dataRequest['avatar'] = $img;
                session()->put(self::SESSION_STORE, $dataRequest);
            }
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $dataSave = $request->all();
            unset($dataSave['_token']);
            $this->adminUpload->modelCreate($dataSave);
            return redirect()->route('index')->with('success', trans('message.add'));
        }
    }

    public function sortNameDesc()
    {
        return redirect()->route('index');
    }

    public function sortNameAsc()
    {
        $condition = [
            ['del_flag', getConstant('DEL_FLAG_ON')]
        ];
        $data = $this->adminRepository->sortNameAsc($condition, 10);
        return view('admin.pages.admin.all', compact('data'));
    }

    public function edit($id)
    {
        $admin = backendGetCurrentUser();
        if ($admin->can('edit', Admin::class)) {
            $edit = $this->adminRepository->find($id);
            $action = url('backend/users/edit/' . $id);
            return view('admin.pages.admin.add_edit', compact('edit', 'action'));
        } else {
            echo "not permision access";
        }
    }

    public function update(Request $request, $id)
    {
        $avatar = $request->file('avatar');
        if ($avatar == " ") {
            $this->validate->ruleUpdate;
            $dataSave = $request->all();
            $this->adminUpload->ModelUpdate($id,$dataSave);
        } else {
            $request->validate($this->validate->ruleUpdate);
            $dataSave = $request->all();
            unset($dataSave['password']);
            unset($dataSave['_token']);
            $this->adminUpload->ModelUpdate($id,$dataSave);
        }
        return redirect()->route('index')->with('success', trans('message.update'));
    }

    public function destroy($id)
    {
        $admin = backendGetCurrentUser();
        if ($admin->can('edit', Admin::class)) {
            $this->adminRepository->delete($id);
            return redirect()->route('index')->with('success', trans('message.delete'));
        }
    }
}
