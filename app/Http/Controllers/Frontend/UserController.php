<?php

namespace App\Http\Controllers\Frontend;

use App\Model\Entities\ResetPassword;
use App\Http\Controllers\Controller;
use App\Repositories\Admin\UserRepository;
use App\Services\SocialNetwork;
use Validator;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Services\AdminUpload;
use App\Mail\SendMail;

class UserController extends Controller
{
    protected $adminUpload;
    protected $userRepository;
    protected $mail;
    protected $social;

    public function __construct()
    {
        $this->userRepository = app(UserRepository::class);
        $this->adminUpload = app(AdminUpload::class);
        $this->mail = app(SendMail::class);
        $this->social = app(SocialNetwork::class);
    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $this->social->linkFB();
        return redirect()->route('home');
    }

    public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallbackGoogle()
    {
        return $this->social->linkGoogle();
    }

    public function create()
    {
        return view('user.register');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:2|max:255',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:2|alpha_num|max:555',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $dataSave = $request->all();
            unset($dataSave['_token']);
            $this->userRepository->create($dataSave);
            $users = $this->userRepository->selectEmail();
            $dataSend = [
                'view' => 'sendemail.create_mail',
                'subject' => 'Send mail create user',
                'data' => $request->all(),
            ];
            foreach ($users as $user) {
                Mail::to($user)->send(new \App\Mail\SendMail($dataSend));
            }
            return redirect('login');
        }
    }

    public function forgot_password()
    {
        return view('user.forgot');
    }

    public function password(Request $request)
    {
        $email = $request->email;
        $user = $this->userRepository->whereEmail($email);
        if ($user == null) {
            return Redirect()->back()->with(['error' => 'Email not found']);
        } else {
            $this->social->linkEmail($email);
            return Redirect()->back()->with(['success' => 'Send Email Succesfully']);
        }
    }

    public function resetPassword(Request $request)
    {
        $timeConfig = getConstant('TOKEN_TIME'); // minutes
        $token = $request->token;
        $id = $request->id;
        $idmh = myDecrypt($id, 1);
        $db = ResetPassword::where([
            'token' => $token,
            'user_id' => $idmh,
            'del_flag' => getConstant('DEL_FLAG_ON'),
        ])->first();
        if (!empty($db)) {
            $timeold = $db->ins_datetime;
            $now = date('Y-m-d H:i:s');
            $timeCheck = date('Y-m-d H:i:s', strtotime($timeold . '+' . $timeConfig . ' minutes'));
            if ($timeCheck < $now) {
                return redirect()->route('loginFacebook')->with('errortime', trans('message.time'));
            } else {
                return view('user.reset_password');
            }
        } else {
            if (empty($db) || $id == null || $token == null) {
                return view('404');
            } else {
                return view('user.reset_password');
            }
        }
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'bail|required|min:5',
            're_password' => 'bail|required|same:password',
        ]);
        $token = $request->token;
        $password = $request->password;
        $this->social->updatePass($password, $token);
        return view('user.login');
    }
}