<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Admin\UserController;
use Closure;
use Auth;
use Validator;
use App\Model\Entities\Admin;

class AdminMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::guard('admin')->user()->role_type == 0) {
                return $next($request);
            } else if (Auth::guard('admin')->user()->role_type == 1) {
                return $next($request);
            } else if (Auth::guard('admin')->user()->role_type == 2) {
                return $next($request);
            }
        }
        return redirect()->route('login');
    }
}
