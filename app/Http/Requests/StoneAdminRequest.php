<?php

namespace App\Http\Requests;

use http\Env\Request;
use Illuminate\Foundation\Http\FormRequest;

class StoneAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//         $rules = [
//            'name' => 'required|min:2|max:255',
//            'email' => "required|unique:users,email",
//            'password' => 'required|min:2|alpha_num|max:555',
//            'avatar' => 'required|mimes:jpg,jpeg,png,bmp,tiff |max:4096'
//        ];
        return  [
            'name' => 'required|min:2|max:255',
            'email' => "required|unique:users,email",
            'password' => 'required|min:2|alpha_num|max:555',
            'avatar' => 'required|mimes:jpg,jpeg,png,bmp,tiff |max:4096'
        ];
    }
    public function after($rules,Request $request)
    {
        $dataRequest = $request->all();
        dd($dataRequest);
        if ($this->somethingElseIsInvalid()) {
            if (!array_key_exists('avatar', $dataRequest)) {
                unset($rules['avatar']);
            }
            $validator = Validator::make($dataRequest,$rules);
            if ($validator->fails()) {
                session()->put(self::SESSION_STORE, $dataRequest);
                $img = $this->adminUpload->uploadImg();
                if (!empty($img)) { //ảnh tồn tại
                    $dataRequest['avatar'] = $img;
                    session()->put(self::SESSION_STORE, $dataRequest);
                }
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $dataSave = $request->all();
                unset($dataSave['_token']);
                $this->adminUpload->modelCreate($dataSave);
                return redirect()->route('index')->with('success', trans('message.add'));
            }
        }
    }
    public function messages()
    {
        return [
            'required' => ':attribute may not be blank',
            'min' => ':attribute Not to be smaller :min',
            'max' => ':attribute Not to be smaller :max',
            'integer' => ':attribute Enter numbers only',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
            'avatar' => 'Photo'
        ];
    }
}
