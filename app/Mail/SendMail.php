<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mailData = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->mailData = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = isset($this->mailData['view']) ? $this->mailData['view'] : '';
        $route = isset($this->mailData['route']) ? $this->mailData['route'] : '';
        $subject = isset($this->mailData['subject']) ? $this->mailData['subject'] : '';
        $data = isset($this->mailData['data']) ? $this->mailData['data'] : [];
        $data['route'] = $route;

        return $this->subject($subject)
            ->view($view, $data);
    }
}
