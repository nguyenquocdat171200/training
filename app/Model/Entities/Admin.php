<?php

namespace App\Model\Entities;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $table = "admin";
    protected $fillable = [
        'name', 'email', 'password', 'role_type', 'ins_id', 'ins_datetime', 'avatar', 'del_flag', 'upd_id', 'upd_datetime'
    ];
    public $timestamps = false;
}
