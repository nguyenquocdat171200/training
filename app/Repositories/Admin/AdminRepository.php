<?php

namespace App\Repositories\Admin;

use App\Model\Entities\User;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;

class AdminRepository extends BaseRepository implements BaseRepositoryInterface
{
    public function __construct(User $admin)
    {
        parent::__construct($admin);
    }
}