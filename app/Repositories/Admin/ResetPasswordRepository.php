<?php

namespace App\Repositories\Admin;

use App\Model\Entities\ResetPassword;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class ResetPasswordRepository extends BaseRepository implements BaseRepositoryInterface
{
    public function __construct(ResetPassword $resetPassword)
    {
        parent::__construct($resetPassword);
    }
}