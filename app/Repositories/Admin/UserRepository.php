<?php

namespace App\Repositories\Admin;

use App\Model\Entities\User;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;

class UserRepository extends BaseRepository implements BaseRepositoryInterface
{
    public function __construct(User $user)
    {
        parent::__construct($user);
    }
}