<?php

namespace App\Repositories;


interface BaseRepositoryInterface
{
    public function getAll($columns = ['*']);

    public function find($id, $columns = ['*']);

    public function paginate($condition = [], $limit = null, $columns = ['*']);

    public function create(array $input);

    public function update(array $input, $id);

    public function delete($id);

    public function sortNameAsc($condition = [], $limit = null, $columns = ['*']);

    public function whereEmail($email);

    public function where($param);

}