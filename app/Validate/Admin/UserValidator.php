<?php

namespace App\Validate\Admin;

use Validator;

class UserValidator
{
    public $rules = [
        'name' => 'required|min:2|max:255',
        'email' => "required|unique:users,email",
        'password' => 'required|min:2|alpha_num|max:555',
        'avatar' => 'mimes:jpg,jpeg,png,bmp,tiff |max:4096'
    ];

    public $ruleUpdate = [
        'name' => 'required|min:2|max:255',
        'email' => 'required|unique:users,email,',
        'avatar' => 'mimes:jpg,jpeg,png,bmp,tiff |max:4096'
    ];
}