<?php
return [
    'DEL_FLAG_ON' => '0', // active
    'DEL_FLAG_OFF' => '1', // deleted
    'PAGINATE' => '10', //pagiante
    'TOKEN_TIME' => '10', //time serial token
    'STATUS_ON' => '0', //active
    'STATUS_OFF' => '1', //not active
];