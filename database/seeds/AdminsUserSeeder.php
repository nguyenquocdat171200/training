<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
class AdminsUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\Model\Entities\Admin();
        $admin->name = 'admin';
        $admin->email = 'datnguyen171200@gmail.com';
        $admin->password = Hash::make('12345');
        $admin->role_type = '0';
        $admin->ins_id = rand(1,1000000);
        $admin->ins_datetime = new DateTime();
        $admin->upd_datetime = new DateTime();
        $admin->del_flag = 0;
        $admin->save();
    }
}
