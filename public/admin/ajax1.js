$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }


});

//Delete product
$('.deleteProduct').click(function () {
    let id = $(this).data('id');
    alert(id);
    $('.delProduct').click(function () {
        $.ajax({
            url: 'users/' + id,
            type: 'delete',
            dataType: 'json',
            success: function (data) {
                toastr.success(data.result, 'Thông báo', {timeOut: 5000});
                $('#delete').modal('hide');
                window.location.reload();
            }
        });
    });
});

//upload ảnh
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#img-photo').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
}

$("#photo").change(function() {
    readURL(this);
});

//tim kiem smart search
var path = "admins";
$('input.typeahead').typeahead({
    source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
            return process(data);
        });
    }
});






