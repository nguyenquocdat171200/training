@extends('admin.master_view')
@section('main')
    @php
        $dataRequest = Session::get('SESSION_STORE');
    @endphp

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User
                <small>Thêm mới</small>
            </h1>
            {{--notification--}}
            @if ($errors->any())
                <div style="margin-top: 20px;" class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if (Session::has('error'))
                <div class="alert alert-info">{{ Session::get('error') }}</div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            {{--end notification--}}
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Tin tức</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements disabled -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">General information</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <form action="{{ $action }}" enctype="multipart/form-data" method="post">
                            {{--                                {!! Form::open(['method'=>'POST','action' => [$action])] !!}--}}
                            {{--                                {!! Form::open(['method' => 'POST', 'action' => $action]) !!}--}}

                            @csrf
                            <!-- text input -->
                                <div class="form-group">
                                    {!!   Form::label('name', 'Name' , ['style'=>'color:red;font-size:18px' ])  !!}
                                    {{--{!! Form::text('name', ) !!}--}}
                                    <input type="text" class="form-control"
                                           name="name" placeholder="Enter ..."
                                           @if(!isset($edit->name))
                                           value="{{old('name')}}"
                                           @elseif(isset($edit->name))
                                           value="{{  isset($edit->name)? $edit->name:'' }}"
                                            @endif

                                    >
                                </div>
                                <div class="form-group">

                                    {!! Form::label('email','Email',['style'=>'color:red;font-size:18px']) !!}
{{--                                    {!! Form::email('email',  (!isset($edit->email) ? old('email') : '' )  , ['class' => 'form-control','placeholder'=>'Enter ...']) !!}--}}
                                    <input type="text" class="form-control"
                                           name="email" placeholder="Enter ..."
                                    @if(!isset($edit->email))
                                        value="{{old('email')}}"
                                    @elseif(isset($edit->email))
                                        value="{{  isset($edit->email)? $edit->email:'' }}"
                                    @endif >
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password','Password',['style'=>'color:red;font-size:18px']) !!}
                                    <input type="password" value="{{old('password')}}" name="password"
                                           class="form-control"
                                           @if(isset($edit->email))
                                           placeholder="Không đổi password thì không nhập thông tin vào ô textbox này"
                                           @else
                                           placeholder="Enter ..."
                                            @endif
                                    >
                                </div>

                                <div class="input-group">
                                    <div class="form-group row">
                                        <div class="col-md-10">
                                            <input type="file" style="width: 100px;background: #ff0000"
                                                   accept="image/x-png,image/gif,image/jpeg"
                                                   data-preview="holder" class="btn btn-primary" id="photo"
                                                   name="avatar"
                                            >
                                        </div>
                                    </div>
                                    @if(isset($edit->avatar) && file_exists('public/img/upload/admin/'.$edit->avatar))
                                        <img id="img-photo" name="avatar"
                                             src="{{asset('public/img/upload/admin')}}/{{$edit->avatar}}"
                                             style="max-width: 200px;">
                                        <input type="hidden" name="hidden_image" value="">
                                    @else
                                        <img id="img-photo"
                                             src="{{ !empty($dataRequest['avatar']) ? asset($dataRequest['avatar']) : asset('public/img/upload/null.png')}}"
                                             style="max-width: 200px;">
                                    @endif
                                </div>
                                <img id="holder" style="margin-top:15px;max-height:100px;">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"
                                                       @if(isset($edit->status) && $edit->status == 0) : checked
                                                       @endif name="status">Status
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="reset" class="btn btn-default">Làm lại</button>
                                    <button type="submit" class="btn btn-info pull-right">Thêm mới</button>
                                </div>

                            </form>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <a style="font-size: 25px;" href="{{ URL::previous() }}"><i
                            class="fa fa-backward btn btn-info pull-left"></i></a>
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop()





