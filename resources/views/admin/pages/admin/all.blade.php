@extends('admin.master_view') @section('main')
    <!-- Content Wrapper. Contains page content -->


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
                <small>List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">User</li>
            </ol>
        </section>

        @if (Session::has('error'))
            <div class="alert alert-info"></div>
        @endif
        @if (Session::has('success'))
            <div class="alert alert-info">{{ Session::get('success') }}</div>
    @endif
    <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="btn-group pull-left" style="margin-top: 15px">
                                <div class="menu-right">
                                    {!! Form::open(['method' => 'GET', 'route' => 'search' ,'id' =>"button_search"]) !!}
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa  fa-search"></i>
                                            <span class="hidden-xs">Search</span>
                                        </button>
                                    </div>
                                    <div class="btn-group pull-right">
                                        {!! Form::text('keyword', isset($_GET['keyword']) ? $_GET['keyword'] : "" ,['class' => 'form-control' , 'placeholder'=>'Search Name....'] ) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div style="float: right;top: 15px;margin-right: 10px" class="dt-buttons btn-group">

                            <a href="{{ url('backend/users/create')}}"
                               style="padding: 5px 10px;margin-left: 5px;font-size: 12px;line-height: 1.5;background: #36c6d3;border-color: #36c6d3;color: #fff!important;"
                               class="btn btn-secondary action-item"><i class="fa fa-plus"></i>
                                Create</span></span>
                            </a>

                            {!! Form::submit('Language',['class' => 'btn btn-secondary buttons-collection dropdown-toggle',
                            'style'=>'padding: 5px 10px;    margin-left: 5px;font-size: 12px;line-height: 1.5;background: #36c6d3;border-color: #36c6d3;color: #fff!important;'
                            ,'tabindex' => 0]); !!}
                            {!! Form::submit('Reload',['class'=>'btn btn-secondary buttons-reload',
                            'style'=>'padding: 5px 10px;margin-left: 5px;font-size: 12px;line-height: 1.5;
                            background: #36c6d3;border-color: #36c6d3;color: #fff!important;'])  !!}

                        </div>
                    </div>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>
                                <a href="{{ url('backend/users/sortNameDesc')}}">
                                    <i style="font-size: xx-large;" class="fa fa-sort-desc" aria-hidden="true"></i>
                                </a>
                                Name
                                <a href="{{ url('backend/users/sortNameAsc')}}">
                                    <i style="font-size: xx-large;" class="fa fa-sort-asc" aria-hidden="true"></i>
                                </a>
                            </th>
                            <th>Email</th>
                            <th style="width: 40px">Avatar</th>
                            <th style="width: 40px">FacebookID</th>
                            <th>Ins_id</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $val)
                            <tr>
                                <td>{{ $val->id }}<span class="label label-primary pull-right"></span></td>

                                <td>{{ $val->name }}</td>
                                <td>{{ $val->email }}</td>
                                <td><img style="width: 80px;height: 80px"
                                         src="{{ asset('public/img/upload/admin') }}/{{ $val->avatar }}">
                                <td>{{ $val->facebook_id }}</td>
                                <td>{{ $val->ins_id }}</td>
                                <td>
                                    <span style="margin-top: 7px;display: inline-block;padding: 1px 10px;color: #fff!important;"
                                          class="label-info status-label">
                                        {{ $val->status == 1 ? "Hiện" : "Ẩn" }}
                                    </span>
                                </td>
                                <th>
                                    <a data-original-title="Edit" data-toggle="tooltip"
                                       href="{{ url('backend/users/edit',$val->id) }}" class="btn btn-primary"> <i
                                                class="fa fa-edit"></i></a>
                                    <a data-original-title="Delete" data-toggle="tooltip"
                                       href="{{ url('backend/users/delete',$val->id) }}"
                                       onclick="return window.confirm('Are you sure?');" class="btn btn-danger"> <i
                                                class="fa fa-trash"></i></a>
                                </th>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th style="width: 40px">Avatar</th>
                            <th style="width: 40px">FacebookID</th>
                            <th>Ins_id</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>

                    <div class="pull-left">{{ $data->links() }}</div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <a style="font-size: 25px;" href="{{ URL::previous() }}"><i
                        class="fa fa-backward btn btn-info pull-left"></i></a>


            <!-- /.col -->
            <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>


    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title" id="exampleModalLabel">

                    </h5>

                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body with-padding">
                    <div>Do you really want to delete this record?</div>
                </div>
                <form>
                    <div class="modal-footer">
                        <button class="float-left btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button class="float-right btn btn-danger  delProduct delete-crud-entry">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop()



